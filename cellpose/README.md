# Cellpose

## Running

```
source /g/almf/software/miniconda3/run_conda.sh
conda activate cellpose-gpu
CUDA_VISIBIBLE_DEVICES=0
```

...and then you could, e.g., run a script or a jupyter notebook.

## Installation

For gpu:
```
conda env create -f environment_gpu.yaml
conda activate cellpose-gpu
pip install --no-deps -r requirements.txt
```

Note that you might need to change the version of cudatoolkit [here](https://git.embl.de/grp-bio-it/ai4ia/-/blob/master/cellpose/environment-gpu.yaml#L3), depending on which cuda version your gpu drivers support.

For cpu:
```
conda env create -f environment_cpu.yaml
conda activate cellpose-cpu
```