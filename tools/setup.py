from setuptools import setup, find_packages

setup(
    name="ai4ia.tools",
    packages=find_packages(),
    version="0.0.1",
    author="Constantin Pape, Christian Tischer",
    url="https://git.embl.de/grp-bio-it/ai4ia.git",
    license='MIT',
    entry_points={
        "console_scripts": [
            "view_data = tools_impl.view_data:main",
            "submit_slurm = tools_impl.submit_slurm:main"
        ]
    },
)
